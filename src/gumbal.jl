# =========================================================== 
# pptools
# --------------------
# This toolbox provides structures and functions for psycho-
# physical experiments. 
# ===========================================================
# Copyright (C) 2022 Matthias Ertl, ertl_matthias@web.de
#
# pptools is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License 
# as published by the Free software Foundation; either 
# version 2 of the License, or (at your option) any later 
# version.
# pptools is distributed in the hope that it will be 
# useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public
# License along with this program; if not, write to the Free
# Software Foundation, Inc., 59 Temple Place, Suite 330, 
# Boston, MA 02111-1 307 USA
# ===========================================================
function gumbal(g::cfg_gumbal, x_values)
  y = g.guess_rate .+ (1 .- g.guess_rate .- g.lapse_rate) .* (1 .- exp.(-1 .* 10 .^(g.beta .* (x_values .- g.alpha))))
end

function gumbal(g::cfg_gumbal, x_values, flag::String)
  if flag == "Inverse"
    c = (x_values-g.gamma) ./ (1 - g.gamma - g.lambda) - 1
    c = -1 .* log.(-1 .* c)
    c = log10.(c)
    y = g.alpha .+ c ./ g.beta
  end
  
  if flag == "Derivative"
    y = (1 - gamma - lambda) .* exp.(-1 .* 10 .^(beta .* (x_values-alpha))) .* log(10) .* 10 .^(beta .* (x_values - alpha)) .* beta
  end
end
