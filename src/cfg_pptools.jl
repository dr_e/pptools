# =========================================================== 
# pptools
# --------------------
# This toolbox provides structures and functions for psycho-
# physical experiments. 
# ===========================================================
# Copyright (C) 2022 Matthias Ertl, ertl_matthias@web.de
#
# pptools is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License 
# as published by the Free software Foundation; either 
# version 2 of the License, or (at your option) any later 
# version.
# pptools is distributed in the hope that it will be 
# useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public
# License along with this program; if not, write to the Free
# Software Foundation, Inc., 59 Temple Place, Suite 330, 
# Boston, MA 02111-1 307 USA
# ===========================================================


abstract type cfg_procedure end

Base.@kwdef mutable struct cfg_staircase <: cfg_procedure 
    up::Int64 = 1
    down::Int64 = 3
    step_size_up::Float64 = .01
    step_size_down::Float64 = .01
    stop_rule::String = "reversals"
    stop_value::Int64 = 32
    start_value::Float64 = 0.0 
    x_max::Float64 = Inf
    x_min::Float64 = -Inf
    truncate::Bool = true
    response::Array{Int64} = []
    stop::Bool = false
    u::Float64 = 0.0
    d::Float64 = 0.0
    direction::Int64 = 0
    reversal::Array{Int64} = [0]
    x_current::Array{Float64} = [start_value]
    x::Array{Float64} = [start_value]
    x_staircase::Array{Float64} = [start_value]
end

Base.@kwdef mutable struct cfg_running_fit <: cfg_procedure 
    prior_alpha_range::Array{Float64} = []
    prior::Array{Float64} = []
    pdf::Array{Float64} = []
    mode::Int = 0
    mean::Float64 = eps()
    mode_uniform_prior::Array{Float64} = []
    mean_uniform_prior::Array{Float64} = []
    sd_uniform_prior::Array{Float64} = []
    response::Array{Int} = []
    stop_rule::String = "trials"
    stop_value::Int = 50
    stop_counter::Int = 0
    PF::Function = pal_gumbel()
    beta::Float64 = 2
    gamma::Float64 = 0.5
    lambda::Float64 = 0.02
    x_max::Float64 = Inf
    x_min::Float64 = -Inf
    direction::Int64 = 0
    reversal::Int = 0
    mean_mode::String = "mean"
    x_current::Array{Float64} = [eps()]
    x::Array{Float64} = [x_current]
    x_staircase::Array{Float64} = [x]
end

Base.@kwdef struct cfg_gumbal  
    alpha::Float64 = 1
    beta::Float64 = 2
    guess_rate::Float64 = .01
    lapse_rate::Float64 = .002
end


