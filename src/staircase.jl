# =========================================================== 
# pptools
# --------------------
# This toolbox provides structures and functions for psycho-
# physical experiments. 
# ===========================================================
# Copyright (C) 2022 Matthias Ertl, ertl_matthias@web.de
#
# pptools is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License 
# as published by the Free software Foundation; either 
# version 2 of the License, or (at your option) any later 
# version.
# pptools is distributed in the hope that it will be 
# useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public
# License along with this program; if not, write to the Free
# Software Foundation, Inc., 59 Temple Place, Suite 330, 
# Boston, MA 02111-1 307 USA
# ===========================================================
function update_sc!(sc::cfg_staircase, response::Int)
  trial = length(sc.x)
  push!(sc.response, response)

  if trial == 1
    push!(sc.x_staircase, sc.x[end])
    if response == 1
      sc.direction = -1
    else
      sc.direction = 1
    end
  end

  if response == 1
    sc.d = sc.d + 1
    if sc.d == sc.down || maximum(sc.reversal) < 1
      push!(sc.x_staircase, sc.x_staircase[end]-sc.step_size_down)
      if sc.x_staircase[end] < sc.x_min && sc.truncate 
        sc.x_staircase[end] = sc.x_min
      end
      sc.u = 0
      sc.d = 0
      
      if sc.direction == 1
        sc.reversal[end] = maximum(sc.reversal) + 1
      else
        sc.reversal[end] = 0
      end
      
      sc.direction = -1
    else
      push!(sc.x_staircase, sc.x_staircase[end])
    end
  else
    sc.u = sc.u + 1
    if sc.u == sc.up || maximum(sc.reversal) < 1
      push!(sc.x_staircase, sc.x_staircase[end]+sc.step_size_up)
      if sc.x_staircase[end] > sc.x_max && sc.truncate
        sc.x_staircase[end] = sc.x_max
      end
      sc.u = 0
      sc.d = 0
      
      if sc.direction == -1
        sc.reversal[end] = maximum(sc.reversal) + 1
      else
        sc.reversal[end] = 0
      end
      
      sc.direction = 1
    else
      push!(sc.x_staircase, sc.x_staircase[end])
    end
  end
  
  push!(sc.reversal, 0)
  
  if sc.stop_rule == "reversals" && maximum(sc.reversal) == sc.stop_value
    sc.stop = 1
  end
  if sc.stop_rule == "trials" && trial == sc.stop_value
    sc.stop = 1
  end

  if ~sc.stop
    push!(sc.x, sc.x_staircase[end])
    if sc.x[end] > sc.x_max
      push!(sc.x, sc.x_max)
    elseif sc.x[end] < sc.x_min
      push!(sc.x, sc.x_min)
    end
    push!(sc.x_current, sc.x[end])
  end

  return sc
end

