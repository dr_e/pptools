# pptools

pptools is a toolbox providing functions for psychophysical experiments. At this point only simple staircases are implemented. 
A mature toolbox implemented in matlab is palamedes (https://www.palamedestoolbox.org/) by Nicolaas Prins and Frederick Kingdom
