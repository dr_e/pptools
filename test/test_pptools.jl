# =========================================================== 
# pptools
# --------------------
# This toolbox provides structures and functions for psycho-
# physical experiments. 
# ===========================================================
# Copyright (C) 2022 Matthias Ertl, ertl_matthias@web.de
#
# pptools is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License 
# as published by the Free software Foundation; either 
# version 2 of the License, or (at your option) any later 
# version.
# pptools is distributed in the hope that it will be 
# useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public
# License along with this program; if not, write to the Free
# Software Foundation, Inc., 59 Temple Place, Suite 330, 
# Boston, MA 02111-1 307 USA
# ===========================================================

using pptools, Printf, Test

@testset "struct cfg_staircase" begin
  sc = pptools.cfg_staircase()
  @test sc.up == 1
  @test sc.down == 3
  @test sc.step_size_up == .01
  @test sc.step_size_down == .01
  @test sc.stop_rule == "reversals"
  @test sc.stop_value == 32
  @test sc.start_value == 0.0
  @test sc.x_max == Inf
  @test sc.x_min == -Inf
  @test sc.truncate == true
  @test sc.response == []
  @test sc.stop == 0.0
  @test sc.u == 0.0
  @test sc.d == 0.0
  @test sc.direction == 0
  @test sc.reversal == [0]
  @test sc.x_current == [sc.start_value]
  @test sc.x == [sc.start_value]
  @test sc.x_staircase == [sc.start_value]

  pptools.update_sc!(sc, 1)
end

@testset "2down/1up - trial" begin
  responses = [ 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0,
                1, 1, 1, 0, 1, 1, 0, 1, 1, 1]
  exp_inten = [1.1000, 0.9000, 0.7000, 0.5000, 0.7000, 0.9000,
               1.1000, 1.1000, 0.9000, 0.9000, 0.7000, 0.7000,
               0.5000, 0.7000, 0.9000, 1.1000, 1.1000, 0.9000,
               0.9000, 1.1000, 1.1000, 0.9000, 1.1000, 1.1000,
               0.9000, 0.9000]
  exp_rever = [ 0, 0, 0, 1, 0, 0, 0, 2, 0, 0, 0, 0, 3, 0, 0,
                0, 4, 0, 5, 0, 6, 7, 0, 8]

  sc = pptools.cfg_staircase(up = 1,
                             down = 2,
                             step_size_up = 0.2,
                             step_size_down = 0.2,
                             stop_rule = "trials", 
                             stop_value = 25,
                             start_value = 1.1)

  for x = 1:length(responses)
    sc = pptools.update_sc!(sc, responses[x])
    @test sc.response[x] == responses[x]
    @test round(sc.x[x], digits = 4) == round(exp_inten[x], digits = 4)
    #@test sc.xStaircase == [x]
  end
  @test sc.reversal[1:length(exp_rever)] == exp_rever
end


@testset "3down/1up - trial" begin
  responses = [1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1]
  exp_inten = [0.3000, 0.2500, 0.2000, 0.1500, 0.1000, 0.0500, 0.0000, -0.0500, 0.0000, 0.0000, 0.0000, -0.0500, -0.0500, -0.0500, 0.0000, 0.0000, 0.0500, 0.0500, 0.1000, 0.1000]
  exp_rever = [0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 2, 0, 0, 3, 0, 0, 0, 0]

  sc = pptools.cfg_staircase(up = 1,
                             down = 3,
                             step_size_up = 0.05,
                             step_size_down = 0.05,
                             stop_rule = "trials", 
                             stop_value = 20,
                             start_value = 0.3)

  for x = 1:length(responses)
    sc = pptools.update_sc!(sc, responses[x])
    @test sc.response[x] == responses[x]
    @test round(sc.x[x], digits = 4) == round(exp_inten[x], digits = 4)
    #@test sc.xStaircase == [x]
  end
  @test sc.reversal[1:length(exp_rever)] == exp_rever
end


@testset "3down/1up - reversal" begin
  responses = [1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1,
               1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1,
               0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1,
               0, 0, 1, 1, 1, 0]
  exp_inten =[15,14,13,12,11,10, 9,10,10,10, 9,10,10,11,11,
              11,10,10,10, 9,10,10,10, 9, 9,10,11,11,11,10,
              10,11,11,11,10,10,10, 9,10,11,11,11,10,10,10,
               9,10,11,11,11,10,11]
  exp_rever = [0, 0, 0, 0, 0, 0, 1, 0, 0, 2, 3, 0, 0, 0, 0,
               4, 0, 0, 0, 5, 0, 0, 6, 0, 7, 0, 0, 0, 8, 0,
               9, 0, 0,10, 0, 0, 0,11, 0, 0, 0,12, 0, 0, 0,
              13, 0, 0, 0,14,15]

  sc = pptools.cfg_staircase(up = 1,
                             down = 3,
                             step_size_up = 1,
                             step_size_down = 1,
                             stop_rule = "reversals", 
                             stop_value = 15,
                             start_value = 15)

  for x = 1:length(responses)
    sc = pptools.update_sc!(sc, responses[x])
    @test sc.response[x] == responses[x]
    @test round(sc.x[x], digits = 4) == round(exp_inten[x], digits = 4)
    #@test sc.xStaircase == [x]
  end
  @test sc.reversal[1:length(exp_rever)] == exp_rever
end

@testset "2down/1up - reversal" begin
  responses = [ 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1]
  exp_rever = [ 0, 1, 0, 2, 0, 0, 0, 3, 0, 4, 0, 5, 0, 0, 6]
  exp_inten = [11,10,11,11,10,10, 9, 9,10,10, 9, 9,10,11,11,10]

  sc = pptools.cfg_staircase(up = 1,
                             down = 2,
                             step_size_up = 1,
                             step_size_down = 1,
                             stop_rule = "reversals", 
                             stop_value = 6,
                             start_value = 11)

  for x = 1:length(responses)
    sc = pptools.update_sc!(sc, responses[x])
    @test sc.response[x] == responses[x]
    @test round(sc.x[x], digits = 4) == round(exp_inten[x], digits = 4)
    #@test sc.xStaircase == [x]
  end
  @test sc.reversal[1:length(exp_rever)] == exp_rever
end

@testset "3down/2up - reversal" begin
  responses = [ 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1,
                1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1,
                0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1,
                1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0,
                1, 1, 1, 0, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1,
                1, 0, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1,
                1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 1, 0, 1, 0, 0,
                0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1,
                0, 0, 1, 0, 1, 1]
  exp_rever = [ 0, 0, 1, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 3, 0, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0,
                5, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 7, 0,
                0, 8, 0, 9, 0, 0,10, 0,11, 0, 0,12, 0, 0,13,
                0, 0,14, 0,15, 0, 0, 0,16, 0, 0, 0,17, 0, 0,
               18, 0, 0,19, 0, 0,20, 0, 0, 0,21, 0, 0,22, 0,
                0, 0, 0,23, 0, 0, 0, 0, 0,24, 0, 0, 0,25, 0,
                0, 0, 0,26, 0, 0, 0, 0, 0,27, 0, 0, 0,28, 0,
                0,29, 0, 0, 0,30]
  exp_inten = [1.1000, 1.0000, 0.9000, 1.0000, 1.0000, 1.0000,
               1.0000, 0.9000, 0.9000, 0.9000, 0.8000, 0.8000,
               0.8000, 0.7000, 0.7000, 0.7000, 0.7000, 0.8000,
               0.8000, 0.9000, 0.9000, 1.0000, 1.0000, 1.0000,
               0.9000, 0.9000, 0.9000, 0.8000, 0.8000, 0.8000,
               0.8000, 0.9000, 0.9000, 0.9000, 1.0000, 1.0000,
               1.1000, 1.1000, 1.1000, 1.0000, 1.0000, 1.0000,
               0.9000, 0.9000, 1.0000, 1.0000, 1.0000, 0.9000,
               0.9000, 1.0000, 1.0000, 1.0000, 0.9000, 0.9000,
               1.0000, 1.0000, 1.0000, 0.9000, 0.9000, 0.9000,
               1.0000, 1.0000, 1.0000, 0.9000, 0.9000, 1.0000,
               1.0000, 1.0000, 1.0000, 0.9000, 0.9000, 0.9000,
               0.9000, 1.0000, 1.0000, 1.0000, 0.9000, 0.9000,
               0.9000, 1.0000, 1.0000, 1.0000, 0.9000, 0.9000,
               0.9000, 0.9000, 1.0000, 1.0000, 1.0000, 0.9000,
               0.9000, 0.9000, 0.8000, 0.8000, 0.9000, 0.9000,
               1.0000, 1.0000, 1.0000, 1.0000, 0.9000, 0.9000,
               0.9000, 0.9000, 1.0000, 1.0000, 1.1000, 1.1000,
               1.1000, 1.0000, 1.0000, 1.0000, 0.9000, 0.9000,
               0.9000, 1.0000, 1.0000, 1.0000, 1.0000, 0.9000,
               0.9000, 0.9000, 1.0000, 1.0000, 1.0000, 1.0000,
               0.9000]

  sc = pptools.cfg_staircase(up = 2,
                             down = 3,
                             step_size_up = .1,
                             step_size_down = .1,
                             stop_rule = "reversals", 
                             stop_value = 30,
                             start_value = 1.1)

  for x = 1:length(responses)
    sc = pptools.update_sc!(sc, responses[x])
    @test sc.response[x] == responses[x]
    @test round(sc.x[x], digits = 8) == round(exp_inten[x], digits = 8)
    #@test sc.xStaircase == [x]
  end
  @test sc.reversal[1:length(exp_rever)] == exp_rever
end

@testset "struct cfg_staircase" begin
  sc = pptools.cfg_staircase()
  @test sc.up == 1
  @test sc.down == 3
  @test sc.step_size_up == .01
  @test sc.step_size_down == .01
  @test sc.stop_rule == "reversals"
  @test sc.stop_value == 32
  @test sc.start_value == 0.0
  @test sc.x_max == Inf
  @test sc.x_min == -Inf
  @test sc.truncate == true
  @test sc.response == []
  @test sc.stop == 0.0
  @test sc.u == 0.0
  @test sc.d == 0.0
  @test sc.direction == 0
  @test sc.reversal == [0]
  @test sc.x_current == [sc.start_value]
  @test sc.x == [sc.start_value]
  @test sc.x_staircase == [sc.start_value]

  pptools.update_sc!(sc, 1)
end

@testset "pdf_descrition" begin
  in_pdf[1:401] = 0.0025
  in_values = -2:0.01:2
end
